import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
public class Instrum {
 String nameOfTest[] = new String [10];
 String finishingComment[] = new String [10];
 String fileName = "Test.txt";
 String additionalComment;
 long startTime[] = new long[10];
 long finishTime[] = new long[10];
 long elapsedTime[] = new long[10];
 double durationInMs[] = new double[10];
 double totalTimeTaken;
 
 
 
boolean activation = false;
 
 
 public void activate (boolean yn) { 
	 if ( yn == true) { 
		 activation = true;
	 } else { 
		 activation = false;
	 }
 }
	
 public void startTiming (String comment, int index) { 
if(activation == false) { 
	 startTime[index] = 0;	
	 }
	if(comment != null) {
	 nameOfTest[index] = comment; 
	}
	 long tempStart = System.nanoTime();
	 startTime[index] = tempStart;	 
	 
	 
 }
 
 public void stopTiming (String comment, int index) { 
if(activation == false) { 
	finishTime[index] = 0;
	 }
	 if(comment != null) {
	 finishingComment[index] = comment; 
	 }
	 long tempFinish = System.nanoTime();
	 finishTime[index] = tempFinish;	 
 }
 
 public void timeDelta (long[] startTime, long[] finishTime, int index) {

	 long elapse = finishTime[index] - startTime[index]; 
	 elapsedTime[index] = elapse;
 }
	
 public void comment (String comment) { 

	 additionalComment = comment; 
 }
 
 
 public void formatter() { 
if(activation == false) { 
		 
	 }

	 for(int i = 0; i < elapsedTime.length; i++) { 
		 timeDelta(startTime,finishTime, i);
		
		 durationInMs[i] = (double)elapsedTime[i]/1000000.000;
		 totalTimeTaken += durationInMs[i];
	 }
	 
 }
 public void dump() throws FileNotFoundException {
if(activation == false) { 
		 
	 }
	
	 PrintWriter out = new PrintWriter(fileName);
	 formatter();	
	 String strToOutput[] = new String [6];
	for(int i = 0; i < 6; i++) { 
		 strToOutput[i] = "STARTTIMING: " + nameOfTest[i] + System.lineSeparator() + finishingComment[i] + " " + durationInMs[i] + System.lineSeparator();
		
	}
	
	
 out.println(Arrays.toString(strToOutput).replace(",","").replace("[", "").replace("]", "") + " Total time taken " + totalTimeTaken + System.lineSeparator() + additionalComment);
	 
	out.close();
	 
 }


}
