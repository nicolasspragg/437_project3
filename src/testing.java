import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Random;

public class testing {
static int arr1[] = new int [10000];
static int arr2[] = new int [10000];
	public static void main(String[] args) throws FileNotFoundException {
		Instrum tester = new Instrum();
		tester.activate(true);
		int total = 0; 
		
		tester.startTiming("timer overhead", 1);
		tester.startTiming("Main function",0);
		tester.stopTiming("time overhead", 1);
		
		
		tester.startTiming("For loop", 2);
		
		
		for (int i = 0; i < 10000; i++) {
	         total += i;
	      }
		tester.stopTiming("for loop ", 2);
		
		
		
		
		tester.startTiming("fill arrays", 3);
		fillArrays();
		tester.stopTiming("fill arrays", 3);
		
		
		
		tester.startTiming("bubbleSort()", 4);
		bubbleSort(arr1);
		tester.stopTiming("bubbleSort()", 4);
		
		
		
		tester.startTiming("quickSort()", 5);
		 partition(arr2,0, arr1.length -1);
		 quickSort(arr2, 0, arr1.length-1);
		tester.stopTiming("quickSort()", 5);
		
		
		
		
		
		
		
		tester.stopTiming("Main function ended",0);
		
		tester.comment("testing completed");
		tester.dump();

	}
	
	public static void fillArrays() { 
		
		
		for(int i = 0; i < arr1.length; i++) {
			arr1[i] = randomFill();
			
		}
		
		for(int i = 0; i < arr2.length; i++) {
			arr2[i] = randomFill();
			
		}
		
	}
	
	 public static int randomFill(){

		    Random rand = new Random();
		    int randomNum = rand.nextInt(100000);
		    return randomNum;
	}
	
	 
	 static void bubbleSort(int[] arr) {  
	        int n = arr.length;  
	        int temp = 0;  
	         for(int i=0; i < n; i++){  
	                 for(int j=1; j < (n-i); j++){  
	                          if(arr[j-1] > arr[j]){  
	                                 //swap elements  
	                                 temp = arr[j-1];  
	                                 arr[j-1] = arr[j];  
	                                 arr[j] = temp;  
	                         }  
	                          
	                 }  
	         }  

}
	 
	 public static int partition(int arr[], int left, int right)
	 {
	       int i = left, j = right;
	       int tmp;
	       int pivot = arr[(left + right) / 2];
	      
	       while (i <= j) {
	             while (arr[i] < pivot)
	                   i++;
	             while (arr[j] > pivot)
	                   j--;
	             if (i <= j) {
	                   tmp = arr[i];
	                   arr[i] = arr[j];
	                   arr[j] = tmp;
	                   i++;
	                   j--;
	             }
	       };
	      
	       return i;
	 }
	  
	public static void quickSort(int arr[], int left, int right) {
	       int index = partition(arr, left, right);
	       if (left < index - 1)
	             quickSort(arr, left, index - 1);
	       if (index < right)
	             quickSort(arr, index, right);
	 }
	 
	 
	 
	 
	 
	 
}
